# README #

This is a test about a network of routes to various ports,

### How do I get set up? ###

After clone the project

Create a new Virtualenv, Python 2 [(install virtualenv)](https://virtualenv.pypa.io/en/stable/installation/).

Activate it, go in django_routes folder, install requirements and migrate the db

```
pip install -r requirements.txt
python manage.py migrate
```

create the data
```
python manage.py start_import
```

and check the questions!
```
python manage.py make_question
```

### Who do I talk to? ###

* Marco Carminati, carminati.marco@gmail.com