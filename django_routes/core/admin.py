from django.contrib import admin

from .models import Port, Route

"""
Admin section.
"""

admin.site.register(Port)
admin.site.register(Route)