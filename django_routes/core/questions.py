from constants import PORT
from utils import check_total_journey, find_short_journey, get_data


class bcolors:
    OKBLUE = '\033[94m'
    ENDC = '\033[0m'


def one():
    """
    What is the total journey time for the following direct routes (your model should indicate if the journey is invalid):

    BuenosAires > NewYork > Liverpool
    BuenosAires > Casablanca > Liverpool
    BuenosAires > CapeTown >NewYork > Liverpool > Casablanca
    BuenosAires > CapeTown >Casablanca
    :return:
    """
    print bcolors.OKBLUE + "What is the total journey time for the following direct routes " \
                           "(your model should indicate if the journey is invalid [result: -1]):" + bcolors.ENDC

    print "BuenosAires > NewYork > Liverpool"
    print check_total_journey([PORT.BUENOSAIRES, PORT.NEWYORK, PORT.LIVERPOOL])

    print "BuenosAires > Casablanca > Liverpool"
    print check_total_journey([PORT.BUENOSAIRES, PORT.CASABLANCA, PORT.LIVERPOOL])

    print "BuenosAires > CapeTown > NewYork > Liverpool > Casablanca"
    print check_total_journey(
        [PORT.BUENOSAIRES, PORT.CAPETOWN, PORT.NEWYORK, PORT.LIVERPOOL, PORT.CASABLANCA])

    print "BuenosAires > CapeTown > Casablanca"
    print check_total_journey([PORT.BUENOSAIRES, PORT.CAPETOWN, PORT.CASABLANCA])


def two():
    """
    Find the shortest journey time for the following routes:
    BuenosAires > Liverpool
    NewYork > NewYork
    :return:
    """
    print bcolors.OKBLUE + "Find the shortest journey time for the following routes:" + bcolors.ENDC
    print "BuenosAires > Liverpool"
    short_days, short_route_list = find_short_journey(PORT.BUENOSAIRES, PORT.LIVERPOOL)
    print "%(short_route_list)s in %(short_days)s days" % dict(short_route_list=short_route_list,
                                                                short_days=short_days)

    print "NewYork > NewYork"
    short_days, short_route_list = find_short_journey(PORT.NEWYORK, PORT.NEWYORK)
    print "%(short_route_list)s in %(short_days)s days" % dict(short_route_list=short_route_list,
                                                                short_days=short_days)


def three():
    """
    Find the number of routes from Liverpool to Liverpool with a maximum number of 3 stops.
    :return:
    """
    print bcolors.OKBLUE + "Find the number of routes from Liverpool to Liverpool with a maximum number of 3 stops." + bcolors.ENDC

    routes = get_data(PORT.LIVERPOOL, PORT.LIVERPOOL)
    for route in routes:
        if route['stops'] <= 3:
            print '%(route_code)s in %(stops)s stop(s)' % dict(route_code=route['route_code'],
                                                                stops=route['stops'])


def four():
    """
    Find the number of routes from Buenos Aires to Liverpool where exactly 4 stops are made.
    :return:
    """
    print bcolors.OKBLUE + "Find the number of routes from Buenos Aires to Liverpool " \
                           "where exactly 4 stops are made." + bcolors.ENDC
    routes = get_data(PORT.BUENOSAIRES, PORT.LIVERPOOL)
    for route in routes:
        if route['stops'] == 4:
            print '%(route_code)s in %(stops)s stop(s)' % dict(route_code=route['route_code'],
                                                                stops=route['stops'])


def five():
    """
    Find the number of routes from Liverpool to Liverpool where the journey time is less than or equal to 25 days.
    :return:
    """
    print bcolors.OKBLUE + "Find the number of routes from Liverpool to Liverpool where the journey time is less " \
                           "than or equal to 25 days." + bcolors.ENDC
    routes = get_data(PORT.LIVERPOOL, PORT.LIVERPOOL)
    for route in routes:
        if route['days'] <= 25:
            print '%(route_code)s in %(days)s days(s)' % dict(route_code=route['route_code'], days=route['days'])
