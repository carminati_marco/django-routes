from django.core.management.base import BaseCommand
from core.questions import *

class Command(BaseCommand):
    """
    Make the test's question.
    This command avoid to open the shell and call the function.
    """

    help = "Let's make the test's question"

    def handle(self, *args, **options):
        """
        Just call the methods to obtain the results.
        """
        one()
        two()
        three()
        four()
        five()

        self.stdout.write(self.style.SUCCESS('Dit it!'))