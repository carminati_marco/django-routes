from django.core.management.base import BaseCommand
from core.models import Port, Route
from core.constants import PORT_LIST, ROUTE_LIST


class Command(BaseCommand):
    """
    Simple command to create Ports' + Routes' list.
    """

    help = 'Create the base of port and route'

    def handle(self, *args, **options):

        # delete all Port + Route
        Port.objects.all().delete()
        Route.objects.all().delete()

        for PORT in PORT_LIST:
            Port.objects.create(code=PORT['code'], name=PORT['name'])

        for ROUTE in ROUTE_LIST:
            from_port = Port.objects.get(code=ROUTE['from_port'])
            to_port = Port.objects.get(code=ROUTE['to_port'])
            Route.objects.create(from_port=from_port, to_port=to_port, days=ROUTE['days'])

        self.stdout.write(self.style.SUCCESS('Port and Route imported!'))