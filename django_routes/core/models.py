# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Port(models.Model):
    """
    Simple Port object with code a name
    """
    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.code


class Route(models.Model):
    """
    Route object.
    It contains from - to port + no. days
    """
    from_port = models.ForeignKey('Port', on_delete=models.CASCADE, related_name='fligths_departures')
    to_port = models.ForeignKey('Port', on_delete=models.CASCADE, related_name='fligths_arrivals')
    days = models.IntegerField()

    def __unicode__(self):
        return '%(from_port)s - %(to_port)s' % dict(from_port=self.from_port, to_port=self.to_port)
