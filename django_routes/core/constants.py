"""
In this file there are data to create the graphs.
If you want to modify it, just change the route_list and call "python manage.py start_import".
"""


class PORT:
    NEWYORK = "NYC"
    LIVERPOOL = "LIV"
    CASABLANCA = "CAS"
    CAPETOWN = "CPT"
    BUENOSAIRES = "BSA"


PORT_LIST = [dict(code=PORT.NEWYORK, name='New York'),
                dict(code=PORT.LIVERPOOL, name='Liverpool'),
                dict(code=PORT.CASABLANCA, name='Casablanca'),
                dict(code=PORT.CAPETOWN, name='Cape Town'),
                dict(code=PORT.BUENOSAIRES, name='Buenos Aires')]

ROUTE_LIST = [dict(from_port=PORT.BUENOSAIRES, to_port=PORT.NEWYORK, days=6),
               dict(from_port=PORT.BUENOSAIRES, to_port=PORT.CASABLANCA, days=5),
               dict(from_port=PORT.BUENOSAIRES, to_port=PORT.CAPETOWN, days=4),
               dict(from_port=PORT.NEWYORK, to_port=PORT.LIVERPOOL, days=4),
               dict(from_port=PORT.LIVERPOOL, to_port=PORT.CASABLANCA, days=3),
               dict(from_port=PORT.LIVERPOOL, to_port=PORT.CAPETOWN, days=6),
               dict(from_port=PORT.CASABLANCA, to_port=PORT.LIVERPOOL, days=3),
               dict(from_port=PORT.CASABLANCA, to_port=PORT.CAPETOWN, days=6),
               dict(from_port=PORT.CAPETOWN, to_port=PORT.NEWYORK, days=8),
               ]
