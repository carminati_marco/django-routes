import unittest

from core.constants import *
from core.models import *
from core.utils import *


class TestCheckout(unittest.TestCase):

    def _start(self):
        # delete all Port + Route
        Port.objects.all().delete()
        Route.objects.all().delete()

        for PORT_CODE in PORT_LIST:
            Port.objects.create(code=PORT_CODE['code'], name=PORT_CODE['name'])

        for ROUTE in ROUTE_LIST:
            from_port = Port.objects.get(code=ROUTE['from_port'])
            to_port = Port.objects.get(code=ROUTE['to_port'])
            Route.objects.create(from_port=from_port, to_port=to_port, days=ROUTE['days'])

    def test_total_journey(self):
        self._start()

        self.assertEqual(check_total_journey([PORT.BUENOSAIRES, PORT.NEWYORK, PORT.LIVERPOOL]), 10)
        self.assertEqual(check_total_journey([PORT.BUENOSAIRES, PORT.CASABLANCA, PORT.LIVERPOOL]), 8)
        self.assertEqual(check_total_journey(
            [PORT.BUENOSAIRES, PORT.CAPETOWN, PORT.NEWYORK, PORT.LIVERPOOL, PORT.CASABLANCA]), 19)
        self.assertEqual(check_total_journey([PORT.BUENOSAIRES, PORT.CAPETOWN, PORT.CASABLANCA]), -1)

    def test_shortest_journey(self):
        self._start()

        short_days, short_route_list = find_short_journey(PORT.BUENOSAIRES, PORT.LIVERPOOL)
        self.assertEqual(short_days, 8)

        short_journey, short_route_list = find_short_journey(PORT.NEWYORK, PORT.NEWYORK)
        self.assertEqual(short_journey, 18)

    def test_number_of_stops(self):
        self._start()

        min_day, max_day = None, None
        min_stop, max_stop = None, None

        for data in get_data(PORT.BUENOSAIRES, PORT.CAPETOWN):
            if min_day is None or min_day > data['days']:
                min_day = data['days']
            if min_stop is None or min_stop > data['stops']:
                min_stop = data['stops']
            if max_day is None or max_day < data['days']:
                max_day = data['days']
            if max_stop is None or max_stop < data['stops']:
                max_stop = data['stops']

        self.assertEqual(min_stop, 1)
        self.assertEqual(max_stop, 4)
        self.assertEqual(min_day, 4)
        self.assertEqual(max_day, 19)
