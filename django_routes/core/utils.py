from core.models import Route
from django.core.exceptions import ObjectDoesNotExist

from graphs import find_all_paths, graph, find_all_node_paths


def cast_to_route_list(li):
    """
    Giving a list of routes, it creates a list using Route models.
    :param li: route_list_code
    :return:
    """
    route_list = []
    for i in range(len(li)):
        if i < len(li) - 1:
            from_port_code = li[i]
            to_port_code = li[i + 1]

            try:
                route_list.append(Route.objects.get(from_port__code=from_port_code,
                                                      to_port__code=to_port_code))
            except ObjectDoesNotExist:
                raise ObjectDoesNotExist

    return route_list


def _get_data(route_list_code):
    """
    givin a route_list, F(x) returns days, stops, route_list.
    total journey time for the following direct routes
    :param route_list: ex. ['BSA', 'NYC', 'LIV']
    :return: -1 if the route_list is invalid
    """
    days = 0

    try:
        # obtain list of route.
        route_list = cast_to_route_list(route_list_code)
    except ObjectDoesNotExist:
        return -1, -1, None

    for route in route_list:
        days += route.days

    stops = len(route_list)
    return days, stops, route_list


def check_total_journey(route_list_code):
    """
    givin a route_list, F(x) returns no_journey.
    total journey time for the following direct routes
    :param route_list: ex. ['BSA', 'NYC', 'LIV']
    :return: -1 if the journey is invalid
    """
    days, stops, route_list = _get_data(route_list_code)
    return days


def _find_routes(from_port_code, to_port_code):
    """
    it obtains the list of different route_list from graph,
    checking if start and end points are different.
    :param from_port_code:
    :param to_port_code:
    :return:
    """

    if from_port_code == to_port_code:
        l_route_list_code = find_all_node_paths(graph, from_port_code)
    else:
        l_route_list_code = find_all_paths(graph, from_port_code, to_port_code)

    return l_route_list_code


def find_short_journey(from_port_code, to_port_code):
    """
    It find the short journey between start and end.
    :param from_port_code:
    :param to_port_code:
    :return:
    """
    # init + get fligths.
    short_days = None
    short_route_list = None
    l_route_list_code = _find_routes(from_port_code, to_port_code)

    # loop of list-route.
    for route_list_code in l_route_list_code:
        journey = check_total_journey(route_list_code)  # get no journey.

        # check if journey < short_days and set result.
        if short_days is None or journey < short_days:
            short_days = journey
            short_route_list = route_list_code

    return short_days, short_route_list


def get_data(from_port_code, to_port_code):
    """
    Obtain data giving start and end port.
    :param from_port_code:
    :param to_port_code:
    :return:
    """
    # init.
    result = []
    l_route_list_code = _find_routes(from_port_code, to_port_code)

    # loop the route list code and gets data.
    for route_list_code in l_route_list_code:
        days, stops, route = _get_data(route_list_code)

        result.append(dict(days=days, stops=stops, route=route, route_code=route_list_code))

    return result
