"""
Function regarding graphs.
To solve the graph problem, we start with use function starting from
https://www.python.org/doc/essays/graphs/
https://codereview.stackexchange.com/questions/55767/finding-all-paths-from-a-given-graph
# todo: check if use also days as weight for routes
"""
from models import Port


def create_graph():
    """
    F(x) that creates a graph with ports (node) and routes (edge).
    The structure must be:
    graph = {'A': ['B', 'C'],
         'B': ['C', 'D'],
         'C': ['D'],
         'D': ['C'],
         'E': ['F'],
         'F': ['C']}
    """
    graph = dict()

    for port in Port.objects.all():  # for port i found in db
        # set for aiport (node) route_departure's to_port (edges).
        graph[port.code] = [route.to_port.code for route in port.fligths_departures.all()]

    return graph


# graph will be used in most of f(x).
graph = create_graph()


def find_all_paths(graph, start, end, path=[]):
    """
    F(x) that obtains all paths giving start + end
    :return: list of obj.
    """
    path = path + [start]
    if start == end:
        return [path]
    if not graph.has_key(start):
        return []
    paths = []
    for node in graph[start]:
        if node not in path:
            newpaths = find_all_paths(graph, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths


def _find_all_node_paths(graph, v):
    """Generate the maximal cycle-free paths in graph starting at v.
    graph must be a mapping from vertices to collections of
    neighbouring vertices.
    NOTE: ONE WAY paths are excluded.

    Find all path from a given path.
    >>> g = {1: [2, 3], 2: [3, 4], 3: [1], 4: []}
    >>> sorted(paths(g, 1))
    [[1, 2, 3], [1, 2, 4], [1, 3]]
    >>> sorted(paths(g, 3))
    [[3, 1, 2, 4]]

    """

    path = [v]  # path traversed so far
    seen = {v}  # set of vertices in path

    def search():
        dead_end = True
        for neighbour in graph[path[-1]]:
            if neighbour not in seen:
                dead_end = False
                seen.add(neighbour)
                path.append(neighbour)
                for p in search():
                    yield p
                path.pop()
                seen.remove(neighbour)
        if dead_end:
            yield list(path)

    for p in search():
        yield p


def _find_direct_route(from_port_code):
    """
    Find all route "ONE WAY" where the route is direct
    :param from_port_code:
    :return:
    """
    result = []
    for i in graph[from_port_code]:
        if from_port_code in graph[i]:
            result.append([from_port_code, i, from_port_code])

    return result


def find_all_node_paths(graph, from_port_code):
    """
    Find all routes where start and and are same.
    :param graph:
    :param from_port_code:
    :return:
    """
    # obtain all node_path > 1 stop
    result = sorted(_find_all_node_paths(graph, from_port_code))
    for r in result:
        r.append(from_port_code)

    # obtain all node_path with 1 stop
    result += _find_direct_route(from_port_code)

    return result
